docker:
	docker build --progress plain -t sneak/go-apt-cacher .

run:
	docker run -p 8080:8080 -ti sneak/go-apt-cacher
