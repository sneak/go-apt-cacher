FROM golang:1.15-buster AS builder

WORKDIR /usr/local/src
#RUN apt update && apt install -y git

RUN git clone https://github.com/cybozu-go/aptutil.git && \
    cd aptutil && \
    git checkout 3f82d83844818cdd6a6d7dca3eca0f76d8a3fce5

WORKDIR /usr/local/src/aptutil
RUN go mod download
WORKDIR /usr/local/src/aptutil/cmd/go-apt-cacher
RUN go build
RUN find .

FROM debian:buster-slim AS final
COPY --from=builder /usr/local/src/aptutil/cmd/go-apt-cacher/go-apt-cacher /usr/local/bin/go-apt-cacher
ADD ./go-apt-cacher.toml /etc/go-apt-cacher.toml
RUN chmod ugoa+rx /usr/local/bin/* && \
    mkdir -p /var/spool/go-apt-cacher && \
    chown nobody:nogroup /var/spool/go-apt-cacher
USER nobody:nogroup
ENTRYPOINT ["/usr/local/bin/go-apt-cacher"]
